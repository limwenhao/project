const { ErrorHandler } = require('../error');

const multer  = require('multer');
const crypto = require('crypto');
const path = require('path');

const config = require('../config');

exports.isAuthenticated = async (req, res, next) => {
  console.log(req.session.user_sid);
  if (!req.session.user_sid){
    console.log('Middleware', 'NO_SESSION_FOUND');
    res.status(401).send('Not authorize');
  } else {
    res.locals.UID = req.session.user_sid;
    res.locals.role = req.session.role;
    res.locals.session = req.session;
    next();
  }
}

exports.catchAsync = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };

const storage = multer.diskStorage({
  destination: function(req, file, callback) {
    let dir = config.imgPath;
    // let dir = __dirname + './../../uploads/';
    callback(null, dir);
  },
  filename: function(req, file, callback){
    let ext = path.extname(file.originalname).toLocaleLowerCase();
    // let filename = crypto.createHash("sha256").update(file.originalname + Date.now()).digest("hex") + ext;
    let filename = req.body.name + ext;

    if(ext !== '.png' && ext !== '.jpg'
      && ext !== '.gif' && ext !== '.jpeg'
      && ext !== '.svg' && ext !== '.bmp'
    ) {
      return callback(new Error('Only images are allowed'))
    }

    callback(null, filename);
  }
});

const arrayUpload = multer({
  storage,
  limits: { fileSize: config.photoFileSize }
}).array('avatar_path', config.photoUploadLimits);

const singleUpload = multer({
  storage,
  limits: { fileSize: config.photoFileSize }
}).single('avatar_path');

exports.uploadSingle = (req, res, next) => {
  singleUpload(req, res, err => {
    if (err instanceof multer.MulterError) {
      console.log('SINGLE_UPLOAD_FILE', err);
      next(new ErrorHandler(400, err.message))
      // return res.status(400).send(err.message);
    } else if (err) {
      console.log('SINGLE_UPLOAD_FILE', err);
      next(new ErrorHandler(400, 'Invalid file format'))
      // return res.status(400).send('Invalid file format');
    }

    next();
  })
};


exports.uploadArray = (req, res, next) => {
  arrayUpload(req, res, err => {
    if (err instanceof multer.MulterError) {
      console.log('UPLOAD_FILE', err);
      next(new ErrorHandler(400, err.message))
      // return res.status(400).send(err.message);
    } else if (err) {
      console.log('UPLOAD_FILE', err);
      next(new ErrorHandler(400, 'Invalid file format'))
      // return res.status(400).send('Invalid file format');
    }
    next();
  })
};
