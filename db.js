const mysql = require('mysql2');
const util = require('util');
const dotenv = require('dotenv');

const envConfiguration = dotenv.config();

if(envConfiguration.error) {
  console.error(envConfiguration.error);
}

const databaseConfigs = {
  host:'localhost',
  // socketPath: '/var/run/mysqld/mysqld.sock',
  user: envConfiguration.parsed.DB_USER,
  password: envConfiguration.parsed.DB_PASS,
  database: envConfiguration.parsed.DB_DATABASE,
  port: envConfiguration.parsed.DB_PORT,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
  multipleStatements: true
  // multipleStatements: true
};

console.log(databaseConfigs);

const dbConnectionPool = mysql.createPool(databaseConfigs);
const promisePool = dbConnectionPool.promise();

dbConnectionPool.getConnection(function(err, connection) {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST')
      console.error('Database connection was closed.');
    if (err.code === 'ER_CON_COUNT_ERROR')
      console.error('Database has too many connections.');
    if (err.code === 'ECONNREFUSED')
      console.error('Database connection was refused.');
  }

  if (connection) connection.release();

  return ;
});

dbConnectionPool.query = util.promisify(dbConnectionPool.query);

module.exports = {
  db: dbConnectionPool,
  promisePool
};
