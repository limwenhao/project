const express = require('express');
const redis = require('redis');
const logger = require('morgan');
const cors = require('cors');
const http = require('http');
const dotenv = require('dotenv');
const path = require('path');

const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const redisStore = require('connect-redis')(session);
const client = redis.createClient();

const api = require('./routes/api');
const { handleError } = require('./error');

const app = express();
const port = 8090;

const envConfiguration = dotenv.config();
if (envConfiguration.error) {
  console.error(envConfiguration.error);
}

// Redis session setting
app.use(session({
  key: 'sid',
  secret: envConfiguration.parsed.SESSION_SECRECT,
  store: new redisStore({
    host: envConfiguration.parsed.REDIS_HOST,
    port: envConfiguration.parsed.REDIS_PORT,
    client: client,
    ttl: 260
  }),
  saveUninitialized: false,
  rolling: true,
  resave: true,
  cookie: {
    // domain:'.localhost',
    // sameSite: 'lax',
    httpOnly: true,
    // secure: true,
    expires: 1 * 3600000
  }
}));

const whiteList = ['http://localhost:3000']
app.use(cors({
  credentials: true,
  origin: whiteList,
  methods: 'GET,PUT,POST,DELETE'
}));

app.enable('trust proxy');

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(logger(':date[web] :status :method :url :response-time ms - :res[content-length]'));

app.use('/api', api);
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use((err, req, res, next) => {
  if (!err.statusCode) {
    console.error(err);
    err.statusCode = 500;
    err.message = 'Unexpected error';
  }

  handleError(err, res);
});

const httpServer = http.createServer(app).listen(port, () => console.log("Service Running on 8090", new Date()));
