const moment = require('moment');

const config = require('./config');
const agentModel = require('./models/agent.js');


const returnStatus = ( result, err ) => {
  return {result, err}
};

exports.validateLoginUserName = (name, fields='') => {
  let errMessage = false;

  if (!name || /\s/g.test(name)) {
    errMessage = 'Username cannot get blank';
  }
  else if (name.length > 3000) errMessage = 'Too long for Username';

  return returnStatus(name, errMessage);
};

exports.validateUserName = (name, fields='') => {
  let errMessage = false;

  if (!name) errMessage = 'Name cannot be blank: ' + fields;
  else if (name.length > 3000) errMessage = 'Too long for name: ' + fields;

  return returnStatus(name, errMessage);
};

exports.validateEmail = email => {
  let errMessage = false;
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!re.test(String(email))) {
    errMessage = 'Wrong email format';
  }

  return returnStatus(String(email).toLowerCase(), errMessage);
};

exports.validatePhoneNumber = tel => {
  let errMessage = false;
  const re = /^[0-9]*$/;

  if (!re.test(String(tel))) {
    errMessage = 'Wrong tel format';
  }

  return returnStatus(String(tel), errMessage);
};

exports.validateRating = rating => {
  let errMessage = false;

  if (typeof rating !== 'number') {
    errMessage = 'Rating must be number';
  }

  if (rating < 1 || rating > 5){
    errMessage = 'Rating must between 1-5';
  }

  return returnStatus(rating, errMessage);
};

exports.validatePassword = pass => {
  let errMessage = false;

  if (pass.length < 6 || !pass.length) errMessage = 'Invalid password';

  return returnStatus(pass, errMessage);
};

exports.validateIsEmptyData = (data, fields='') => {
  let errMessage = false;

  if (!data || data == ' ') errMessage = 'Data cannot be empty: ' + fields;

  return returnStatus(data, errMessage);
};

//tools function
exports.checkAgentExist = async(validAgentID) => {
  let result
  result = await agentModel.getAgentExist(validAgentID);
  if (result.length > 0) {
    return true
  } 
  else {
    return false
  }
 
}