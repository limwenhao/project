const bcrypt = require('bcryptjs');
const { ErrorHandler } = require('../error');
const validations = require('../validations');

// models
const userModel = require('../models/users.js');

const SALTROUNDS = 10;

const hashPassword = (password) => {
  return new Promise((res, rej) => {
    bcrypt.genSalt(SALTROUNDS, (err, salt) => {
      if (err) rej("bcrypt.genSalt error : " + err);
      bcrypt.hash(password, salt, (err, hash) => {
        if (err) rej("bcrypt.hash error : " + err);
        res(hash);
      });
    });
  });
};

exports.register = async (req, res, next) => {
  const {username, password, role_id, status} = req.body;
  let dbResult, dbUserResult;
  let validUsername = validations.validateLoginUserName(username);
  let validPass = validations.validatePassword(password);

  let validateArr = [validUsername, validPass];

  try {
    validateArr.forEach(i => {
      if (i.err) throw new ErrorHandler(400, i.err);
    });

  } catch (err) {
    console.log('CREATE_USER', err);
    return next(err);
  }
  
  try {
    dbUserResult = await userModel.getUserByName(validUsername.result);
  } catch (err) {
    console.error('CREATE_USER', err);
    return next(new ErrorHandler(501, err));
  }

  if (dbUserResult.length) {
    console.error('CREATE_USER', 'Username exits:', username);
    return next(new ErrorHandler(400, 'Username already in use'));
  }

  // TODO Assign Role
  try {
    const encrypt = await hashPassword(password);
    dbResult = await userModel.createUser([encrypt, validUsername.result, status ? status : 0, role_id]);

  } catch (err) {
    console.error('CREATE_USER_DB', err);
    return next(new ErrorHandler(502, 'Unexpected Error'));
  }

  if (!dbResult) {
      console.log(dbResult)
    console.error('CREATE_USER_DB', err);
    return next(new ErrorHandler(503, 'Unexpected Error'));
  }

  // TODO: Handle Save Role to Session
  // req.session.user_sid = dbResult;
  // req.session.user_role = 'role';
  res.sendStatus(201);
}

exports.login = async (req, res, next) => {
  const {username, password} = req.body;
  let dbResult;
  let validName = validations.validateLoginUserName(username);
  let validPass = validations.validatePassword(password);
  
  let validateArr = [validName, validPass];

  try {
    validateArr.forEach(i => {
      if (i.err) throw i.err;
    });

  } catch (err) {
    console.log('LOGIN_USER', err);
    return next(new ErrorHandler(400, err));
  }

  try {
    dbResult = await userModel.getUserByName(validName.result);
  } catch (err) {
    console.error('CREATE_USER', err);
    return next(new ErrorHandler(500, err));
  }

  const userProfile = dbResult[0];
  try {
    // Account not found
    if (!dbResult.length) throw 'Account not found';

    // Password incorrect
    let hashedPassword = userProfile.password;
    let isPasswordCorrect = await bcrypt.compare(password, hashedPassword);
    if (!isPasswordCorrect) throw 'Incorrect password';

    // Account haven't verified
    if (userProfile.status !== 1) throw 'Account not verified';

  } catch (err) {
    console.log('LOGIN_USER', err);
    return next(new ErrorHandler(400, 'Incorrect account or password'));
  }

  req.session.user_sid = userProfile.id;
//   req.session.user_username = userProfile.userName;

  // TODO: Return role details?
  const details = {
    ID: userProfile.id,
    username: userProfile.userName,
    status: userProfile.status
  };

  res.status(200).send({details})
}

exports.logout = async (req, res, next) => {
  req.session.destroy(function (err) {
    if (err) {
      console.error(err);
    } else {
      res.sendStatus(204);
    }
  });
}

exports.getUser = async (req, res, next) => {
  const userId = req.session.user_sid;
  
  // TODO: Get User Details
  try {
    dbResult = await userModel.getUser([userId]);
  } catch (err) {
    console.log('GET_USER', err);
    return next(new ErrorHandler(500, err));
  }

  if (!dbResult.length) {
    console.log('GET_USER', 'User not found');
    return next(new ErrorHandler(404, 'User not found'));
  }

  const user = dbResult[0];
  const details = {
    user
  }

  res.status(200).send({details});
}

exports.updateUserPassword = async (req, res, next) => {
  const { currentPassword, newPassword } = req.body;
  const userId = req.session.user_sid;

  let validPass = validations.validatePassword(newPassword);
  
  try {
    if (validPass.err) throw validPass.err;
  } catch (err) {
    console.log('UPDATE_USER_PASSWORD', err);
    return next(new ErrorHandler(400, err));
  }
    
   try {
    dbResult = await userModel.getUser([userId]);
  } catch (err) {
    console.error('UPDATE_USER_PASSWORD', err);
    return next(new ErrorHandler(500, err));
  }

  try {
    if (!dbResult.length) throw 'Account not found';
    let hashedPassword = dbResult[0].password;
    let isPasswordCorrect = await bcrypt.compare(currentPassword, hashedPassword);
    if (!isPasswordCorrect) throw 'Incorrect password';
  }
  catch (err) {
    console.log('UPDATE_USER_PASSWORD', err);
    return next(new ErrorHandler(400, err));
  }

  const encrypt = await hashPassword(newPassword);
  const input = [encrypt, userId];
  try {
    await userModel.updateUserPassword(input);
  } catch (err) {
    console.error('UPDATE_USER_STATUS', err);
    return next(new ErrorHandler(500, err));
  }
  res.sendStatus(204)
}