const { ErrorHandler } = require('../error');
const validations = require('../validations');

// models
const agentModel = require('../models/agent.js');
const commentModel = require('../models/comment.js');

exports.createAgent = async (req, res, next) => {
    const {name, tel, description} = req.body;
    const files = req.files;
    
    let validUsername = validations.validateLoginUserName(name);
    let validTel = validations.validatePhoneNumber(tel);
    let validCmt = validations.validateIsEmptyData(description);

    let validateArr = [validUsername, validTel, validCmt];

    try {
        validateArr.forEach(i => {
        if (i.err) throw new ErrorHandler(400, i.err);
        });

    } catch (err) {
        console.log('CREATE_AGENT', err);
        return next(err);
    }

    let filesName = [];
    if (files) {
        const splitPath = files[0].path.split('.');
        const fileTypes = splitPath[1];
        const tmpFilename = `${name}.${fileTypes}`;
        // const _fileName = { path: tmpFilename };

        const uploadsString = 'uploads/';
       
        filesName = uploadsString + `${tmpFilename}`

    }

    // TODO Assign Role
    try {
        dbResult = await agentModel.createAgent([name, tel, description, filesName]);
    } catch (err) {
        console.error('CREATE_AGENT_DB', err);
        return next(new ErrorHandler(502, 'Unexpected Error'));
    }

    if (!dbResult) {
        console.log(dbResult)
        console.error('CREATE_AGENT_DB', err);
        return next(new ErrorHandler(503, 'Unexpected Error'));
    }

    res.sendStatus(201);
}

exports.getAgent = async (req, res, next) => {
    const {agentID} = req.query;
    // TODO: Get Agent Details
    try {
      dbResult = await agentModel.getAgent([agentID]);
    } catch (err) {
      console.log('GET_AGENT', err);
      return next(new ErrorHandler(500, err));
    }
  
    if (!dbResult.length) {
      console.log('GET_AGENT', 'Agent not found');
      return next(new ErrorHandler(404, 'Agent not found'));
    }
  
    const agent = dbResult[0];

  
    res.status(200).send({agent});
}

exports.updateAgent = async (req, res, next) => {
    const {agentID, name, tel, description, avatar_path} = req.body;
    const files = req.files;

    let validName = validations.validateUserName(name);
    let validTel = validations.validatePhoneNumber(tel);
    let validCmt = validations.validateIsEmptyData(description);

    let validateArr = [validName, validTel, validCmt];

    let filesName = [];
    if (files) {
        const splitPath = files[0].path.split('.');
        const fileTypes = splitPath[1];
        const tmpFilename = `${name}.${fileTypes}`;
        // const _fileName = { path: tmpFilename };

        const uploadsString = 'uploads/';
       
        filesName = uploadsString + `${tmpFilename}`

    }

    try {
    validateArr.forEach(i => {
        if (i.err) throw new ErrorHandler(400, i.err);
    });
    } catch (err) {
    console.log('UPDATE_AGENT', err);
    return next(err);
    }

    try {
    let arr = [
        name,
        tel,
        description,
        filesName,
        agentID
    ]
    await agentModel.updateAgent(arr);
    } catch (err) {
    console.error('UPDATE_AGENT', err);
    return next(new ErrorHandler(500, err));
    }
    res.sendStatus(204)
}

exports.deleteAgent = async (req, res, next) => {
    const {agentID} = req.body;
    
    try {
      await agentModel.deleteAgent([agentID]);
    } catch (err) {
      console.error('DELETE_AGENT', err);
      return next(new ErrorHandler(500, err));
    }
  
    res.sendStatus(204);
}

exports.getAgentList = async (req, res, next) => {
    const { search, limit, offset, orderBy, orderType, cmt_limit,cmt_offset, cmt_orderBy, cmt_orderType} = req.query;
  
    let _offset = limit * (offset - 1);
  
    const obj = {
      search,
      orderBy,
      orderType,
      limit,
      offset: _offset,
    }
  
    try {
      dbResult = await agentModel.getAgentList(obj);
    } catch (err) {
      console.error('GET_AGENT', err);
      return next(new ErrorHandler(500, err));
    }
  
  
    let agentList = dbResult[0];
    if(agentList.length > 1){
      try{
        await Promise.all(agentList.map(async item=>{
          const id = item.id;
          const cmt_obj = {
            id,
            limit : cmt_limit,
            offset : cmt_offset,
            orderBy : cmt_orderBy,
            orderType : cmt_orderType
          }
          var commentlist = await commentModel.getCommentList(cmt_obj);
          item.comment_details = commentlist[0];
          item.totalComment = commentlist[1][0].totalComment;
          return item;
        }))
      }catch (err){
        console.log('GET_COMMENT',err);
        return next(new ErrorHandler(500,err));
      }
    }
    
    const totalAgent = dbResult[1][0].totalAgent;
    const details = {
      agentList,
      totalAgent
    }
  
    res.status(200).send({details});
}