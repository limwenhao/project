const { ErrorHandler } = require('../error');
const validations = require('../validations');

// models
const commentModel = require('../models/comment.js');

exports.createComment = async (req, res, next) => {
    const {agentID, comment, rating, user_email} = req.body;
    
    let validEmail = validations.validateEmail(user_email);
    let validRating = validations.validateRating(rating);
    let validComment = validations.validateIsEmptyData(comment);

    let validateArr = [validEmail,validRating,validComment];
    
   try{
    const checkAgent = await validations.checkAgentExist(agentID);
    
    if(!checkAgent){
        return next(new ErrorHandler(404, 'Agent Not Found'));
    }
   } catch (err){
        return next(new ErrorHandler(500,err));
   }

    try {
        validateArr.forEach(i => {
        if (i.err) throw new ErrorHandler(400, i.err);
        });

    } catch (err) {
        console.log('CREATE_COMMENT', err);
        return next(err);
    }

    // TODO Assign Role
    try {
        dbResult = await commentModel.createComment([agentID,validComment.result,validRating.result,validEmail.result]);
    } catch (err) {
        console.error('CREATE_COMMENT_DB', err);
        return next(new ErrorHandler(502, 'Unexpected Error'));
    }

    if (!dbResult) {
        console.log(dbResult)
        console.error('CREATE_COMMENT_DB', err);
        return next(new ErrorHandler(503, 'Unexpected Error'));
    }

    res.sendStatus(201);
}

exports.getCommentList = async (req, res, next) => {
    const { limit, offset, orderBy, orderType} = req.query;
  
    let _offset = limit * (offset - 1);
  
    const obj = {
      orderBy,
      orderType,
      limit,
      offset: _offset,
    }
  
    try {
      dbResult = await commentModel.getCommentList(obj);
    } catch (err) {
      console.error('GET_Comment_List', err);
      return next(new ErrorHandler(500, err));
    }
  
  
    const commentList = dbResult[0];
    const totalComment = dbResult[1][0].totalComment;
    const details = {
      commentList,
      totalComment
    }
  
    res.status(200).send({details});
}