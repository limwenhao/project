let config = {};

config.imgPath = __dirname + '/uploads';
config.imgUrl = '';

config.photoUploadLimits = 1;
config.photoFileSize = 5 * 1000 * 1000;

module.exports = config;
