API

- user
- register
- login
- logout
- update admin password

- get agent list
- create agent
- update agent
- delete agent
- read agent
- upload agent avatar

- comment
- get comment list
- create comment

- role
- get role list
- create role
- update role
- delete role
