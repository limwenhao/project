var express = require('express');
var router = express.Router();

const commentController = require('../controllers/comment');
const {catchAsync} = require('../middlewares');


router.post('/', 
    catchAsync(commentController.createComment)
)

router.get('/',
    catchAsync(commentController.getCommentList)
)

module.exports = router;
