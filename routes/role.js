var express = require('express');
var router = express.Router();


router.post('/', 
    rolecontroller.createRole
)

router.get('/',
    rolecontroller.getRole
)

router.get('/role-list',
    rolecontroller.getRoleList
)

router.put('/',
    rolecontroller.updateRole
)

router.delete('/',
    rolecontroller.deleteRole
)

module.exports = router;
