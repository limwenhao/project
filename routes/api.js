const express = require('express');
const router = express.Router();

// middleware

// routes
const users = require('./users');
const agent = require('./agent');
const comment = require('./comment');
// const role = require('./role');

router.use('/users', users);
router.use('/agent', agent);
router.use('/comment', comment);
// router.use('/role', role);

module.exports = router;
