var express = require('express');
var router = express.Router();

const agentController = require('../controllers/agent');
const {uploadArray,catchAsync, isAuthenticated} = require('../middlewares');

router.post('/create', 
    catchAsync(isAuthenticated),
    uploadArray,
    catchAsync(agentController.createAgent)
)

router.put('/',
    catchAsync(isAuthenticated),
    uploadArray,
    catchAsync(agentController.updateAgent)
)

router.get('/',
    catchAsync(agentController.getAgent)
)

router.get('/agent-list',
    catchAsync(agentController.getAgentList)
)

router.delete('/',
    catchAsync(isAuthenticated),
    catchAsync(agentController.deleteAgent)
)


module.exports = router;