var express = require('express');
var router = express.Router();

const userController = require('../controllers/users');
const {catchAsync, isAuthenticated} = require('../middlewares');


router.post('/create', 
  catchAsync(userController.register)
)

router.post('/',
  catchAsync(userController.login)
)

router.post('/logout',
 catchAsync(userController.logout)
)

router.get('/',
 catchAsync(isAuthenticated),
 catchAsync(userController.getUser)
)

router.put('/update-password',
  catchAsync(isAuthenticated),
  catchAsync(userController.updateUserPassword)
);

// router.put('/',
//  catchAsync(userController.updateUser)
// )

module.exports = router;
