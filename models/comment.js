const { db, promisePool } = require('../db.js');

/**
 * Create a new comment
 * @param data [agent_id, comment, rating, user_email]
 */
 exports.createComment = async(data) => {
    try{
      const connection = await promisePool.getConnection();
      await connection.beginTransaction();
  
      try {
        const sql = "INSERT INTO comment (agent_id, comment, rating, user_email) VALUES(?,?,?,?);";
        result = await connection.query(sql, data);
        let resourceID = result[0].insertId;
  
        status = resourceID;
        await connection.commit();
      } catch (err) {
        await connection.rollback();
        status = false;
        // Throw the error again so others can catch it.
        throw err;
      } finally {
        connection.release();
      }
    }
    catch (err) {
      console.error(err);
      status = false;
    }
    return status;
}

/**
 * Get comment list details by id
 * @param data {id, limit, offset, orderBy, orderType}
 */
 exports.getCommentList = data => {
  let sql = "SELECT SQL_CALC_FOUND_ROWS agent_id, agent_id, comment, rating, user_email, create_at "+
    "FROM comment ";
    let _paramSql = "";
  if(data.id){
    _paramSql += `WHERE agent_id = ${data.id} `;
  }


  let _orderBy = null;
  switch (data.orderBy) {
    case 'rating':
      _orderBy = 'rating';
      break;
    case 'date':
      _orderBy = 'create_at';
      break;
    default:
      _orderBy = 'rating';
      break;
  }
  if (_orderBy) {
    _paramSql += `ORDER BY ${_orderBy} `;
    if (data.orderType) {
      _paramSql += data.orderType;
    }
  }

  if (data.limit != null && data.offset != null) _paramSql = _paramSql + ` LIMIT ${data.limit} OFFSET ${data.offset} `;

  sql = sql + _paramSql + "; SELECT FOUND_ROWS() AS totalComment;";
  
  return db.query(sql, data);
};
