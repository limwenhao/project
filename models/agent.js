const { db, promisePool } = require('../db.js');
const path = require('path');

/**
 * Create a new agent
 * @param data [name, tel, description, avatar_path]
 */
 exports.createAgent = async(data) => {
    try{
      const connection = await promisePool.getConnection();
      await connection.beginTransaction();
  
      try {
        const sql = "INSERT INTO agent (name, tel, description, avatar_path) VALUES(?,?,?,?);";
        result = await connection.query(sql, data);
        let resourceID = result[0].insertId;
  
        status = resourceID;
        await connection.commit();
      } catch (err) {
        await connection.rollback();
        status = false;
        // Throw the error again so others can catch it.
        throw err;
      } finally {
        connection.release();
      }
    }
    catch (err) {
      console.error(err);
      status = false;
    }
    return status;
}

/**
 * Get agent details by id
 * @param data [agentID ]
 */
 exports.getAgent = data => {
    let sql = "SELECT id, name, tel, description, avatar_path AS photo FROM agent WHERE id = ?;";
  
    return db.query(sql, data);
};


/**
 * Update a agent
 * @param data [name, tel, description, avatar_path, id]
 */
 exports.updateAgent = data => {
    let sql = "UPDATE agent" +
      " SET name=?, tel=?, description=?, avatar_path=? WHERE id=?;";
  
    return db.query(sql, data);
}

/**
 * Delete a Agent
 * @param data [id]
 */
 exports.deleteAgent = data => {
    let sql = " DELETE FROM agent WHERE id=?;"
  
    return db.query(sql, data);
}

/**
 * Get agent list details by id
 * @param data {search, limit, offset}
 */
 exports.getAgentList = data => {
    let sql = "SELECT SQL_CALC_FOUND_ROWS agent.id, agent.name, agent.tel, agent.description, agent.avatar_path, IFNULL(CAST((SUM(comment.rating) / Count(comment.id)) AS DECIMAL(12,2)),0) AS avg_rating "+
      "FROM agent " + 
      "LEFT JOIN comment on comment.agent_id = agent.id ";
    let _paramSql = "";

    if (data.search) {
      _paramSql = `WHERE agent.name LIKE '%${data.search}%' `;
    }
  
  
    let _orderBy = null;
    switch (data.orderBy) {
      case 'rating':
        _orderBy = 'avg_rating';
        break;
      case 'name':
        _orderBy = 'name';
        break;
      default:
        break;
    }

    _paramSql += " GROUP BY agent.id ";

    if (_orderBy) {
      _paramSql += `ORDER BY ${_orderBy} `;
      if (data.orderType) {
        _paramSql += data.orderType;
      }
    }
  
    if (data.limit != null && data.offset != null) _paramSql = _paramSql + ` LIMIT ${data.limit} OFFSET ${data.offset} `;
  
    sql = sql + _paramSql + "; SELECT FOUND_ROWS() AS totalAgent;";
    
    return db.query(sql, data);
  };

/**
 * Get same name date
 * @param data [agentID]
 */
 exports.getAgentExist = (data) => {
    let sql = "SELECT name"
      + ` FROM agent WHERE id = ?;`;
  
    return db.query(sql, data);
};
