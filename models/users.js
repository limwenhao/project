const { db, promisePool } = require('../db.js');

/**
 * Get user details by id
 * @param data [userId]
 */
 exports.getUser = data => {
    let sql = "SELECT id, username AS userName, password, status FROM user WHERE id = ?;";
  
    return db.query(sql, data);
  };

/**
 * Create a new user
 * @param data [password, username, status, roleid]
 */
 exports.createUser = async(data) => {
    try{
      const connection = await promisePool.getConnection();
      await connection.beginTransaction();
  
      try {
        const sql = "INSERT INTO user (password, username, status, role_id) VALUES(?,?,?,?);";
        result = await connection.query(sql, data);
        let resourceID = result[0].insertId;
  
        status = resourceID;
        await connection.commit();
      } catch (err) {
        await connection.rollback();
        status = false;
        // Throw the error again so others can catch it.
        throw err;
      } finally {
        connection.release();
      }
    }
    catch (err) {
      console.error(err);
      status = false;
    }
    return status;
  }

/**
 * Get user details by name
 * @param data [displayName]
 */
 exports.getUserByName = data => {
  let sql = "SELECT user.id, user.username AS userName,user.password, user.status " +
    "FROM user WHERE user.username = ?;";

  return db.query(sql, data);
};

/**
 * update user password
 * @param data [password, user_id]
 */
 exports.updateUserPassword = (data) => {

  let sql = "UPDATE user" +
  " SET password=?" +
  " WHERE id=?;";

  return db.query(sql, data);
};